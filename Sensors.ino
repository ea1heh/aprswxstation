/*
 * 
 * Funciones de los sensores BME280/BMP280 y DHT11
 * 
 */

void initSensor() {
  #ifdef SENSORDHT
    dhtSensor.begin();
  #endif
  #ifdef SENSORBME280
    Log  = "\nIniciando BME280 en 0x";
    Log += bmeSensor.begin();
    Log += "...\n";
  #endif
  #ifdef SENSORBMP280
    Log  = "\nIniciando BMP280 en 0x";
    Log += bmpSensor.begin();
    Log += "...\n";
    bmpSensor.setSampling(Adafruit_BMP280::MODE_NORMAL,   
                          Adafruit_BMP280::SAMPLING_X2, 
                          Adafruit_BMP280::SAMPLING_X16,  
                          Adafruit_BMP280::FILTER_X16,  
                          Adafruit_BMP280::STANDBY_MS_500);
  #endif
  
  PRINTLOG;
  delay(10);
                       
}


void printSensor() {
  float pressurePrint;
  getSensorValues();

  #ifdef SENSORBMP280
    pressurePrint = wx.pressure/100;
  #endif
  #ifdef SENSORBME280
    pressurePrint = wx.pressure;
  #endif

  Log  = "Presion atmosferica...: ";
  Log += (pressurePrint);
  Log += "mmHg\n";

  Log  = "Temperatura...........: ";
  Log += (wx.temperatureC);
  Log += "ºC\n";

  Log  = "Humedad...............: ";
  Log += (wx.humidity);
  Log += "%\n";

  Log  = "Luminosidad...........: ";
  Log += ReadLuminosidad();
  Log += "%\n";
  
  PRINTLOG;
}


void getSensorValues(){
    float pres;
    
    #ifdef SENSORBMP280
      wx.temperatureC = bmpSensor.readTemperature();
      if((pres=bmpSensor.readPressure()) > 0.1f) {
    #endif

    #ifdef SENSORBME280
      sensors_event_t temp_event, pressure_event, humidity_event;
      bme_temp->getEvent(&temp_event);
      bme_pressure->getEvent(&pressure_event);
      bme_humidity->getEvent(&humidity_event);
      
      wx.temperatureC = temp_event.temperature;
      if((pres=pressure_event.pressure) > 0.1f) {
    #endif
      wx.pressure = pres * ( pow(1.0 -(0.0065 * (float) station.altitude * -1 / (273.15+wx.temperatureC)), 5.255));
    } else {
      wx.pressure = 0.0f;
    }
    
    wx.temperatureF = (wx.temperatureC*1.8)+32;

    #ifdef SENSORDHT
      wx.humidity  = dhtSensor.readHumidity()+60.0;
    #endif
    #ifdef SENSORBME280
      wx.humidity  = humidity_event.relative_humidity;
    #endif
    
    wx.heatindex = CalcHeatIndex(wx.temperatureC, wx.humidity);
}


//FYI: https://ehp.niehs.nih.gov/1206273/ in detail this flow graph: https://ehp.niehs.nih.gov/wp-content/uploads/2013/10/ehp.1206273.g003.png
float CalcHeatIndex(float temperature, float humidity) {
  float heatIndex(NAN);

  if(isnan(temperature) || isnan(humidity)) {
    return heatIndex;
  }

  temperature = (temperature * (9.0 / 5.0) + 32.0); /*conversion to [°F]*/
 
  // Using both Rothfusz and Steadman's equations
  // http://www.wpc.ncep.noaa.gov/html/heatindex_equation.shtml
  if(temperature <= 40) {
    heatIndex = temperature;  //first red block
  } else {
    heatIndex = 0.5 * (temperature + 61.0 + ((temperature - 68.0) * 1.2) + (humidity * 0.094)); //calculate A -- from the official site, not the flow graph

    if(heatIndex >= 79) {
      /*
      * calculate B  
      * the following calculation is optimized. Simply spoken, reduzed cpu-operations to minimize used ram and runtime. 
      * Check the correctness with the following link:
      * http://www.wolframalpha.com/input/?source=nav&i=b%3D+x1+%2B+x2*T+%2B+x3*H+%2B+x4*T*H+%2B+x5*T*T+%2B+x6*H*H+%2B+x7*T*T*H+%2B+x8*T*H*H+%2B+x9*T*T*H*H
      */
      heatIndex = hi_coeff1
      + (hi_coeff2 + hi_coeff4 * humidity + temperature * (hi_coeff5 + hi_coeff7 * humidity)) * temperature
      + (hi_coeff3 + humidity * (hi_coeff6 + temperature * (hi_coeff8 + hi_coeff9 * temperature))) * humidity;
      //third red block
      if ((humidity < 13) && (temperature >= 80.0) && (temperature <= 112.0)) {
        heatIndex -= ((13.0 - humidity) * 0.25) * sqrt((17.0 - abs(temperature - 95.0)) * 0.05882);
      } else if ((humidity > 85.0) && (temperature >= 80.0) && (temperature <= 87.0)) {
        heatIndex += (0.02 * (humidity - 85.0) * (87.0 - temperature));
      }
    }
  }

  return (heatIndex - 32.0) * (5.0 / 9.0); /*conversion back to [°C]*/
}

                                       
unsigned int ReadLuminosidad(){
  unsigned int raw = 0;
  double lx = 0;
  raw = analogRead(A0);

  lx = ((raw*100)/1024);
  if(lx > 100.00) lx = 100.00;
  if(lx <   0.00) lx = 0.00;

  return (int)lx;
}


void calcDewPoint(){
  // Calcular punto de rocio
  double A0 = 373.15/(273.15 + wx.temperatureF);
  double SUM = -7.90298 * (A0-1);
  SUM += 5.02808 * log10(A0);
  SUM += -1.3816e-7 * (pow(10, (11.344*(1-1/A0)))-1) ;
  SUM += 8.1328e-3 * (pow(10,(-3.49149*(A0-1)))-1) ;
  SUM += log10(1013.246);
  double VP = pow(10, SUM-3) * wx.humidity;
  double T = log(VP/0.61078);   
  wx.fdewptf = (241.88 * T) / (17.558-T);
}
