// Variables de programa
#define DEBUG    1      // Depurar por puerto serie (comentar esta linea para deshabilitar la salida por el puerto serie)
#define BAUDRATE 115200 // Velocidad del puerto serie del ESP8266

// Variables WiFi
#define WIFI_SSID "SSID"
#define WIFI_KEY  "WiFiKey"
#define WIFI_HOST "APRSWXStation"

#define HTTP_PROTECTED // Comentar para no proteger con usuario y contraseña el acceso por Web al ESP8266
#define HTTP_USERNAME "admin"
#define HTTP_PASSWORD "xxxxxxxx"

// Variables APRS
#define APRS_CALLSIGN  "EA1XXX-13"
#define APRS_PASSWORD  11111
#define APRS_LATITUDE  "0000.00N"
#define APRS_LONGITUDE "00000.00W"
#define APRS_ALTITUDE  100
#define APRS_SRV_HOST  "spain2.aprs2.net"
#define APRS_SRV_PORT  14580
#define APRS_BEACON    "<Mi ubicacion> APRS WX Station"
#define APRS_DELAY     10

// Variables MQTT
#define MQTT_ENABLE    1 // Habilitar servidor MQTT. Comentar esta linea para desabilitar el envio de mensajes al servidor MQTT
#define MQTT_HOST      "xxx.xxx.xxx.xxx"
#define MQTT_PORT      1883
#define MQTT_USERNAME  "MQTT_USER"
#define MQTT_PASSWORD  "MQTTPassword"
#define MQTT_DELAY     10

// Variables de los sensores (solo se puede tener activo el BME280 o el BMP280, no los dos a la vez)
#define SENSORBME280    1 
//#define SENSORBMP280    1
//#define SENSORDHT       1
#define DHTTYPE         DHT11  // DHT 11
#define DHTPIN          2      // GPIO2 - D4
