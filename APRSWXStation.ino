/*
 * APRS WX Station v1.0
 * 
 * EA1HEH - Javier
 * 
 * ea1heh@gmail.com
 * 
 * 15/02/2021
 * 
 */

#include "Version.h"
#include "Configuration.h"
#include "HTTPTemplate.h"

#include <Ticker.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>    // Para actualizacion OTA
#include <Wire.h>

#ifdef SENSORBME280
  #include <Adafruit_BME280.h>
#endif

#ifdef SENSORBMP280
  #include <Adafruit_BMP280.h>
#endif

#ifdef SENSORDHT
  #include <DHT.h>
#endif

#ifdef MQTT_ENABLE
  #include <PubSubClient.h>         // Cliente MQTT
  #include <ArduinoJson.h>          // https://github.com/bblanchon/ArduinoJson   // Usar la libreria v6.x
#endif

//*** Coeficientes Heat Index (para la humedad)
#define hi_coeff1 -42.379
#define hi_coeff2   2.04901523
#define hi_coeff3  10.14333127
#define hi_coeff4  -0.22475541
#define hi_coeff5  -0.00683783
#define hi_coeff6  -0.05481717
#define hi_coeff7   0.00122874
#define hi_coeff8   0.00085282
#define hi_coeff9  -0.00000199

// Servidor Web
ESP8266WebServer server(80);

#ifdef SENSORBMP280
  Adafruit_BMP280 bmpSensor;
#endif

#ifdef SENSORBME280
  // Sensor BME280
  Adafruit_BME280 bmeSensor;
  Adafruit_Sensor *bme_temp = bmeSensor.getTemperatureSensor();
  Adafruit_Sensor *bme_pressure = bmeSensor.getPressureSensor();
  Adafruit_Sensor *bme_humidity = bmeSensor.getHumiditySensor();
#endif

#ifdef SENSORDHT
  // Sensor DHT11
  DHT dhtSensor(DHTPIN, DHTTYPE);
#endif

// Cliente WiFi
WiFiClient client;

#ifdef MQTT_ENABLE
// Cliente MQTT
PubSubClient mqttClient(client);

// Estructura con informacion MQTT
typedef struct  {
  char     host[48];
  uint16_t port;
  char     username[16];
  char     password[16];
  long     sendDelay;
} MqttStruct, *MqttStructPtr;
MqttStruct mqttSRV;
Ticker TkMQTT;
bool sendFlagMQTT;

String mqttInTopic;
String mqttOutTopic;
#endif

// Estructura con informacion de la estacion APRS
typedef struct  {
  char callsign[10];
  char tlm_callsign[10];
  char longitude[10];
  char latitude[10];
  char aprsServerHost[30];
  char aprsBeacon[50];
  int  aprsPassword;
  int  altitude;
  int  aprsServerPort;
  long transmitDelay;
} positionStruct, *positionStructPtr;
positionStruct station;    //declare la structure
positionStructPtr station_ptr = &station;
Ticker TkAPRS;
bool sendFlagAPRS;

// Estructura con la informacion de la estacion meteorologica
typedef struct  {
  float  temperatureC;
  float  temperatureF;
  float  pressure;
  float  humidity;
  float  heatindex;
  double fdewptf;
} WeatherStruct, *WeatherStructPtr;
WeatherStruct wx;

// Estructura con la informacion de la telemetria
typedef struct {
  int8_t rssi;
  int8_t luminosidad;
} Telemetry, *TelemetryPtr;
Telemetry tl; // declare telemetry structure
TelemetryPtr tl_ptr = &tl;

// Otras variables globales
uint8_t resetReason = 0;
String Log;


////////////////////////////
// CONFIGURACION DEL ESP8266
void setup(void) {  
  char tmp[15];
  unsigned int len;     // for padding callsign

  resetReason = ESP.getResetInfoPtr()->reason;
  /////////////////////////////////
  // 0 - Power reboot
  // 1 - Hardware WDT reset
  // 2 - Fatal exception
  // 3 - Software WDT reset
  // 4 - Software reset
  // 5 - Deep-sleep
  // 6 - Hardware reset

  strcpy(station.callsign,       APRS_CALLSIGN);
  strcpy(station.longitude,      APRS_LONGITUDE);
  strcpy(station.latitude,       APRS_LATITUDE);
  strcpy(station.latitude,       APRS_LATITUDE);
  strcpy(station.aprsServerHost, APRS_SRV_HOST);
  strcpy(station.aprsBeacon,     APRS_BEACON);
  station.aprsPassword   = APRS_PASSWORD;
  station.altitude       = APRS_ALTITUDE;
  station.aprsServerPort = APRS_SRV_PORT;
  station.transmitDelay  = APRS_DELAY;

#ifdef MQTT_ENABLE
  strcpy(mqttSRV.host,     MQTT_HOST);
  strcpy(mqttSRV.username, MQTT_USERNAME);
  strcpy(mqttSRV.password, MQTT_PASSWORD);
  mqttSRV.port  = MQTT_PORT;
  mqttSRV.sendDelay = MQTT_DELAY;

  strcpy(tmp, APRS_CALLSIGN);
  mqttOutTopic  = "WXStation/";
  mqttOutTopic += String(tmp);
  mqttOutTopic += "/out";
  mqttInTopic  = "WXStation/";
  mqttInTopic += String(tmp);
  mqttInTopic += "/in";
#endif

  // Ajustar el indicativo a maximo 9 caracteres, tal y como es solicitado por el paquete de telemetria
  strcpy(station.tlm_callsign, station.callsign);
  
  if((len=strlen(station.callsign))<9) {
    do{ // Pad with spaces
      station.tlm_callsign[len++] = 0x20; 
    }
    while(len < 9);
  }

#ifdef DEBUG
  Serial.begin(BAUDRATE);
#endif
  Log = "\n"; PRINTLOG;
  delay(2000);
  Log = "\n"; PRINTLOG;

  wifiConnect();
  initSensor();
#ifdef MQTT_ENABLE
  configureMQTT();
#endif
     
  server.on("/",     handleRoot);
  server.on("/info", handleInfo);
  server.on("/rst",  handleReset);
  server.on("/aprs", handleAprs);
  server.on("/up",   handleUpdate);
  server.on("/u1",   handleUpgradeFirmwareStart);  // OTA
  server.on("/u2",   HTTP_POST, handleUploadDone, handleUploadLoop);
  server.onNotFound(handleNotFound);

  const char * headerkeys[] = {"User-Agent","X-Forwarded-For"};
  size_t headerkeyssize = sizeof(headerkeys) / sizeof(char*);
  server.collectHeaders(headerkeys, headerkeyssize);
  server.begin();
  Log = "[WX]: Servidor HTTP iniciado...\n";
  PRINTLOG;

  randomSeed(analogRead(A0));

  // Establecer sendFlag{APRS,MQTT} a true para transmitir cada x minutos
#ifdef MQTT_ENABLE
  sendFlagMQTT = true;
  TkMQTT.attach(mqttSRV.sendDelay*60, SetSendFlagMQTT);
#endif
  sendFlagAPRS = true;
  TkAPRS.attach(station.transmitDelay*60, SetSendFlagAPRS);
}


void wifiConnect() {  
  if(WiFi.status() != WL_CONNECTED) {
    Log  = "\n[WX]: Conectando a ";
    Log += WIFI_SSID;
    PRINTLOG;
    
    WiFi.persistent(false);       // WiFi config isn't saved in flash
    WiFi.mode(WIFI_STA);          // use WIFI_AP_STA if you want an AP
    WiFi.hostname(WIFI_HOST); // must be called before wifi.begin()
    WiFi.begin(WIFI_SSID, WIFI_KEY);

    while(WiFi.status() != WL_CONNECTED) {
      delay(250);
      Log = ".";
      PRINTLOG;
    }
  }

  Log  = "\n[WX]: Conectado a ";
  Log += WIFI_SSID;
  Log += "\n[WX]: Direccion IP: ";
  Log += WiFi.localIP().toString();
  Log += "\n\n";
  PRINTLOG;
}


String showResetReason(int code) {
  String tmp;
  switch(code) {
    case 0: tmp = "Power reboot"; break;
    case 1: tmp = "HW WDT reset"; break;
    case 2: tmp = "Fatal exc"; break;
    case 3: tmp = "SW WDT reset"; break;
    case 4: tmp = "SW reset"; break;
    case 5: tmp = "Deep-sleep"; break;
    case 6: tmp = "HW reset"; break;
    default: tmp = "Unknown"; break;
  }

  return tmp;
}


char *uptime(bool longTxt) {
  return (char *)uptime(longTxt, millis()); 
}


char *uptime(bool longTxt, unsigned long milli) {
  static char _return[32];
  unsigned long secs=milli/1000, mins=secs/60;
  unsigned int hours=mins/60, days=hours/24;
  milli -= secs*1000;
  secs  -= mins*60;
  mins  -= hours*60;
  hours -= days*24;

  //sprintf(_return,"Uptime %d dias %2.2d:%2.2d:%2.2d.%3.3d", (byte)days, (byte)hours, (byte)mins, (byte)secs, (int)milli);
  if(longTxt) {
    sprintf(_return,"%d dias %2.2d:%2.2d:%2.2d", (byte)days, (byte)hours, (byte)mins, (byte)secs);
  } else {
    sprintf(_return,"Uptime %dd%dh", (byte)days, (byte)hours);
  }
  return _return;
}


void SetSendFlagAPRS(void){
  sendFlagAPRS = true;
}


#ifdef MQTT_ENABLE
void SetSendFlagMQTT(void){
  sendFlagMQTT = true;
}
#endif


void loop() {
  server.handleClient();

  // APRS flag
  if(sendFlagAPRS) {
    
    if(WiFi.status() == WL_CONNECTED) {
      getSensorValues();
      Send2APRS();
      sendFlagAPRS = false;
    }
  }

#ifdef MQTT_ENABLE
  // MQTT flag
  if(sendFlagMQTT) {
    if(WiFi.status() == WL_CONNECTED) {
      printSensor();
      Send2MQTT();
      sendFlagMQTT = false;
    }
  }
#endif
  
}
