/*
 * APRS WX Station
 *
 * v1.0 - 25/01/2021 - Version inicial
 * 
 */

#define VERSION "1.0"
#define FECHA   "15/02/2021"

#define PRINTLOG { \
  if(DEBUG) { \
    Serial.print(Log); \
  } \
}
