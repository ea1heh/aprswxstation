# APRSWXStation by EA1HEH
ESP8266 IoT y sensores BME280/BMP280/DTH11 para una estación meteorológica para servidores APRS y MQTT, con actualizacion del codigo via web.

![webpage](https://gitlab.com/ea1heh/aprswxstation/-/raw/master/web.png)

Este proyecto ha empezado insipirado en un fork del trabajo de IU5HKU (https://github.com/IU5HKU/MiniWXStation), en el cual al final se ha utilizado algunas líneas de su código para la realización por completo de este codigo.

Por el momento, este proyecto esta funcionando sin problemas con:
- NodeMCU V0.9 (ESP-12)
- NodeMCU V1.0 (ESP-12E)
- Wemos D1 mini (ESP-12F)
- Sensor BMP280
- Sensor BME280
- Sensor DHT11
- LDR

Un esquema simple de conexión de los sensores a la placa de desarrollo es el siguiente:

![schematic](https://gitlab.com/ea1heh/aprswxstation/-/raw/master/schematic.jpg)

La configuración del programa se centra unicamente en el archivo Configuration.h, que es donde definiremos nuestra configuracion WiFi (hay que tener el servidor DHCP activado en la red, ya que este codigo no da opcion a una configuracion IP con direccionamiento fijo), usuario y contraseña de la pagina de administracion, datos de conexión a un servidor APRS (por defecto spain2.aprs2.net, servidor que tambien administro personalmente desde hace unos años), datos del servidor MQTT y por ultimo, configuración de los sensores disponibles. Hay que tener en cuenta que no podemos usar los dos sensores BME/P 280 simulataneamente ya que sino nos dara un error de compilación. A su vez, el sensor DHT solo veremos su valor si elegimos el sensor BMP280, que carece de sensor de humedad. Si tenemos preseleccionado el sensor BMP280, los datos del DHT11 no serán mostrados.

El software utilizado para compilar este proyecto es Arduino (actualmente version 1.8.13), con las siguientes librerias:
- ArduinoJSON 6.x - https://github.com/bblanchon/ArduinoJson
- Arduino Client for MQTT - https://github.com/knolleary/pubsubclient
- Adafruit BME280
- Adafruit BMP280
- DHT
- Wire
- Ticker

Inicialmente hay que programar la placa ESP8266 via Arduino, las compilaciones posteriores, se puede exportar el binario compilado y subirlo a traves de la web de administracion del ESP8266.

Los parametros MQTT sirven para integrarlo dentro de un servidor Home Assistant, usando la siguiente configuración en el archivo configuration.yaml

```javascript
# APRS WX Station
sensor:
  - platform: mqtt
    state_topic: "WXStation/EA1XXX-13/out"
    name: "Temperatura"
    unit_of_measurement: 'ºC'
    value_template: "{{ value_json.temp | float|round(1) }}"
    device_class: "temperature"
    unique_id: "wxstation.temperature"
  - platform: mqtt
    state_topic: "WXStation/EA1XXX-13/out"
    name: "Humedad"
    unit_of_measurement: '%'
    value_template: "{{ value_json.hum | float|round(1) }}"
    device_class: "humidity"
    unique_id: "wxstation.humidity"
  - platform: mqtt
    state_topic: "WXStation/EA1XXX-13/out"
    name: "Presion Atmosferica"
    unit_of_measurement: 'mbar'
    value_template: "{{ value_json.pres | float|round(1) }}"
    device_class: "pressure"
    unique_id: "wxstation.pressure"
  - platform: mqtt
    state_topic: "WXStation/EA1XXX-13/out"
    name: "Luminosidad"
    unit_of_measurement: '%'
    value_template: "{{ value_json.lx }}"
    device_class: "illuminance"
    unique_id: "wxstation.illuminance"
  - platform: mqtt
    state_topic: "WXStation/EA1XXX-13/out"
    name: "Signal"
    unit_of_measurement: 'dBm'
    value_template: "{{ value_json.rssi }}"
    device_class: "signal_strength"
    unique_id: "wxstation.rssi"
```
![homeassistant](https://gitlab.com/ea1heh/aprswxstation/-/raw/master/homeassistant.png)
