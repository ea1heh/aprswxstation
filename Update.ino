/*
 * Funciones para actualizacion por HTTP del ESP8266
 */

void handleUpdate(void) {
  httpAuth();
  
  String page = FPSTR(HTML_HEADER);
  page.replace("{title}", " - Actualizaci&oacute;n");
  page += FPSTR(HTML_STYLE);
  page += String(F("<script language='javascript'>function httpGet(theUrl){if(window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}else{xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');}xmlhttp.onreadystatechange=function(){if(xmlhttp.readyState==4 && xmlhttp.status==200){return xmlhttp.responseText;}};xmlhttp.open('GET',theUrl,false);xmlhttp.send();return xmlhttp.response;}</script>"));
  page += FPSTR(HTML_HEADER_END);
  page += String(F("<h3 align=\"center\">APRS WX Station - Actualizaci&oacute;n</h3>"));

  page += F("<div style='text-align:left;display:inline-block;color:#000000;min-width:340px;'><div style='text-align:center;'>");
  page += F("<div id='f1' style='display:block;'>");

  /*
  page += F("<fieldset><legend><b>&nbsp;Actualizar por servidor web&nbsp;</b></legend>");
  page += F("<form method='get' action='u1'>");
  page += F("<br><br><b>OTA URL</b><br>");
  page += F("<input id='o' placeholder='OTA URL File' name='otaurl' value='https://gitlab.com/ea1heh/aprswxstation/-/raw/master/aprswxstation.bin?inline'><br><br>");
  page += F("Versi&oacute;n actual: ");
  page += F(VERSION);
  page += F("<br>Versi&oacute;n disponible: <script language=\'javascript\'>document.write(httpGet('https://gitlab.com/ea1heh/aprswxstation/-/raw/master/aprswxstation.bin.version?inline'));</script><br><br>");
  page += F("<button type='submit'>Comenzar actualización</button>");
  page += F("</form>");
  page += F("</fieldset><br><br>");
  */
  
  page += F("<fieldset><legend><b>&nbsp;Actualizar cargando archivo .bin&nbsp;</b></legend>");
  page += F("<form method='post' action='u2' enctype='multipart/form-data'><br>");
  page += F("<input type='file' name='u2'><br><br>");
  page += F("<button type='submit' onclick='eb(\"f1\").style.display=\"none\";eb(\"f2\").style.display=\"block\";this.form.submit();'>Iniciar actualizaci&oacute;n</button>");
  page += F("</form>");
  page += F("</fieldset>");
  
  page += F("</div>");
  
  page += F("<div id='f2' style='display:none;text-align:center;'><b>Cargando archivo ...</b></div>");
  page += F("</div></div>");

  page += F("<form action='/' method='get'><button>Volver</button></form><br/>");
  page += FPSTR(HTML_COPYRIGHT);
  page += FPSTR(HTML_FOOTER);

  server.sendHeader("Content-Length", String(page.length()));
  server.send(200, "text/html", page);
} // handleUpdate()


void handleUpgradeFirmwareStart(void) {
  httpAuth();
  
  String page = FPSTR(HTML_HEADER);
  page.replace("{title}", " - Reset");
  page += FPSTR(HTML_STYLE);
  page += F("<script>setTimeout(function(){location.href='.';},45000);</script>");
  page += FPSTR(HTML_HEADER_END);
  page += String(F("<h3 align=\"center\">APRS WX Station - Actualizaci&oacute;n</h3>"));
  page += F("La estaci&oacute;n meteorologica se esta actualizando.<br><br>No apague ni reinicie el dispositivo.");
  page += FPSTR(HTML_COPYRIGHT);
  page += FPSTR(HTML_FOOTER);

  server.sendHeader("Content-Length", String(page.length()));
  server.send(200, "text/html", page);

  Log = "[WX]: Preparando para actualizar...";
  PRINTLOG;
  t_httpUpdate_return ret = ESPhttpUpdate.update(server.arg("otaurl"));
  yield();

  switch (ret) {
    case HTTP_UPDATE_FAILED:
      Log = "[WX]: HTTP_UPDATE_FAILD Error (";
      Log += ESPhttpUpdate.getLastError();
      Log += "): ";
      Log += ESPhttpUpdate.getLastErrorString().c_str();
      PRINTLOG;
      break;
    case HTTP_UPDATE_NO_UPDATES:
      Log = "[WX]: No hay actualizacion HTTP - ";
      Log += HTTP_UPDATE_NO_UPDATES;
      PRINTLOG;
      break;
    case HTTP_UPDATE_OK:
      Log = "[WX]: Actualizacion correcta, reiniciando..."; 
      PRINTLOG;
      delay(2000);
      ESP.reset();
      delay(2000);
      break;
  }
} // handleUpgradeFirmwareStart()


void handleUploadDone(void) {
  String page = FPSTR(HTML_HEADER);
  page.replace("{title}", " - Actualizacion");
  page += FPSTR(HTML_STYLE);
  page += FPSTR(HTML_HEADER_END);
  page += String(F("<h3 align=\"center\">APRS WX Station - Actualizaci&oacute;n</h3>"));
  
  if(Update.hasError()) {
    page += F("La actualizaci&oacute;n <b>NO</b> se ha instalado.<br><br>");
    page += F("<form action='/' method='get'><button>Volver</button></form><br/>");
  } else {
    page += F("<script>setTimeout(function(){location.href='.';},20000);</script>");
    page += F("La actualizaci&oacute;n se ha instalado satisfactoriamente.<br><br>La estaci&oacute;n meteorol&oacute;gica se esta reiniciando...<br><br>");
  }

  page += FPSTR(HTML_COPYRIGHT);
  page += FPSTR(HTML_FOOTER);
  
  server.sendHeader("Connection", "close");
  server.sendHeader("Content-Length", String(page.length()));
  server.send(200, "text/html", page);
  delay(2000);
  ESP.restart();
  delay(2000);
} // handleUploadDone()


void handleUploadLoop(void) {
  httpAuth();
  
  HTTPUpload& upload = server.upload();
  
  if(upload.status == UPLOAD_FILE_START) {
#ifdef DEBUG
    Serial.setDebugOutput(true);
    Serial.printf("[WX]: Update: %s\n", upload.filename.c_str());
#endif
    uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
    if(!Update.begin(maxSketchSpace)) { //start with max available size
      Update.printError(Serial);
    }
  } else if(upload.status == UPLOAD_FILE_WRITE) {
    if(Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
      Update.printError(Serial);
    }
  } else if(upload.status == UPLOAD_FILE_END) {
    if(Update.end(true)) { //true to set the size to the current progress
#ifdef DEBUG
      Serial.printf("[WX]: Actualizacion exitosa: %u\nReiniciando...\n", upload.totalSize);
    } else {
      Update.printError(Serial);
#endif
    }
#ifdef DEBUG
  Serial.setDebugOutput(false);
#endif
  }
  yield();
} //handleUploadLoop()
