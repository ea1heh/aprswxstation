/*
 * Paginas Web
 */

void handleRoot() {
  String tmp;
  String page = FPSTR(HTML_HEADER);

  httpAuth(); 
  
  getSensorValues();
  page.replace("{title}", VERSION);
  page += FPSTR(HTML_STYLE);
  page += FPSTR(HTML_HEADER_END);

  page += String(F("<h3 align=\"center\">APRS WX Station</h3><br>"));
  page += F("<table style='width:100%'>");

  #ifdef SENSORBMP280
      tmp = (wx.pressure/100);
  #endif
  #ifdef SENSORBME280
      tmp = (wx.pressure);
  #endif
  tmp += "mmHg"; 
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Presi&oacute;n atmosf&eacute;rica: ");
  page.replace("{value}", tmp);

  tmp  = (wx.temperatureC);
  tmp += "&deg;C"; 
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Temperatura:");
  page.replace("{value}", tmp);

  tmp  = wx.humidity;
  tmp += "%"; 
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Humedad:");
  page.replace("{value}", tmp);

  tmp  = ReadLuminosidad();
  tmp += "%"; 
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Luminosidad:");
  page.replace("{value}", tmp);

  page += F("</table>");
  page += String(F("<br><br>"));

  page += String(F("<form action='/aprs' method='get'><button>Enviar trama APRS</button></form><br/>"));
  page += String(F("<form action='/info' method='get'><button>Informaci&oacute;n</button></form><br/>"));
  page += String(F("<form action='/up' method='get'><button>Actualizar firmware</button></form><br/>"));
  page += String(F("<form action='/rst' method='get' onsubmit='return confirm(\"Confirmar Reinicio\");'><button class='button bred'>Reiniciar</button></form><br/>"));
  page += FPSTR(HTML_COPYRIGHT);
  page += FPSTR(HTML_FOOTER);

  server.sendHeader("Content-Length", String(page.length()));
  server.send(200, "text/html", page);
  
  
  server.sendHeader(F("Content-Length"), String(page.length()));
  server.send(200, F("text/html"), page);
}

  
void handleInfo() {
  String tmp;
  httpAuth();
  
  String page = FPSTR(HTML_HEADER);
  page.replace("{title}", " - Informaci&oacute;n");
  page += FPSTR(HTML_STYLE);
  page += FPSTR(HTML_HEADER_END);
  page += String(F("<h3 align=\"center\">APRS WX Station - Informaci&oacute;n</h3>"));
  page += F("<table style='width:100%'>");

  page += FPSTR(HTML_TABLEROW_EMPTY);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Indicativo");
  page.replace("{value}", APRS_CALLSIGN);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Latitud");
  page.replace("{value}", APRS_LATITUDE);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Longitud");
  page.replace("{value}", APRS_LONGITUDE);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Servidor APRS");
  page.replace("{value}", APRS_SRV_HOST);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Beacon");
  page.replace("{value}", APRS_BEACON);

  page += FPSTR(HTML_TABLEROW_EMPTY);  

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Versi&oacute;n del programa");
  page.replace("{value}", VERSION);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Versi&oacute;n Core/SDK");
  //page.replace("{value}", String(ARDUINO_ESP8266_RELEASE)+"/"+ESP.getSdkVersion());
  page.replace("{value}", ESP.getSdkVersion());


  tmp = showResetReason(resetReason);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Causa reinicio");
  page.replace("{value}", tmp);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Uptime");
  page.replace("{value}", uptime(true));

  page += FPSTR(HTML_TABLEROW_EMPTY);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "AP SSID");
  page.replace("{value}", WiFi.SSID());

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Direcci&oacute;n MAC");
  page.replace("{value}", WiFi.macAddress().c_str());

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Direcci&oacute;n IP");
  page.replace("{value}", WiFi.localIP().toString().c_str());

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "M&aacute;scara de red");
  page.replace("{value}", WiFi.subnetMask().toString().c_str());

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Gateway");
  page.replace("{value}", WiFi.gatewayIP().toString().c_str());

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Servidor DNS");
  page.replace("{value}", WiFi.dnsIP().toString().c_str());

#ifdef MQTT_ENABLE
  page += FPSTR(HTML_TABLEROW_EMPTY);
  
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Servidor MQTT");
  page.replace("{value}", mqttSRV.host);

  tmp = String(mqttSRV.port);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Puerto MQTT");
  page.replace("{value}", tmp.c_str());

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Usuario MQTT");
  page.replace("{value}", mqttSRV.username);

  tmp = "APRSWX";
  tmp += String(ESP.getFlashChipId(), HEX);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Cliente MQTT");
  page.replace("{value}", tmp);

  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Out Topic MQTT");
  page.replace("{value}", mqttOutTopic);
#endif

  page += FPSTR(HTML_TABLEROW_EMPTY);

  tmp = String(ESP.getChipId());
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "ESP Chip ID");
  page.replace("{value}", tmp);

  tmp = String(ESP.getFlashChipId(), HEX);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Flash Chip ID");
  page.replace("{value}", "0x"+tmp);

  tmp = String(ESP.getFlashChipRealSize() / 1024);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Flash Chip Size");
  page.replace("{value}", tmp+"Kb");

  tmp = String(ESP.getFlashChipSize() / 1024);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Tama&ntilde;o programa Flash");
  page.replace("{value}", tmp+"Kb");

  tmp = String(ESP.getSketchSize() / 1024);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Tama&ntilde;o programa");
  page.replace("{value}", tmp+"Kb");

  tmp = String(ESP.getFreeSketchSpace() / 1024);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Free Program Space");
  page.replace("{value}", tmp+"Kb");

  tmp = String(ESP.getFreeHeap() / 1024);
  page += FPSTR(HTML_TABLEROW);
  page.replace("{desc}", "Memoria libre");
  page.replace("{value}", tmp+"Kb");

  page += FPSTR(HTML_TABLEROW_EMPTY);
 
  page += F("</table>");
  page += String(F("<form action='/' method='get'><button>Volver</button></form><br/>"));
  page += FPSTR(HTML_COPYRIGHT);
  page += FPSTR(HTML_FOOTER);

  server.sendHeader("Content-Length", String(page.length()));
  server.send(200, "text/html", page);
} // handleInfo()


void handleAprs() {
  httpAuth();
  
  String page = FPSTR(HTML_HEADER);
  page.replace("{title}", " - Enviar APRS");
  page += FPSTR(HTML_STYLE);
  page += F("<script>setTimeout(function(){location.href='.';},9000);</script>");
  page += FPSTR(HTML_HEADER_END);
  page += String(F("<h3 align=\"center\">APRS WX Station - APRS</h3>"));
  page += F("Enviando trama a servidor APRS.");
  page += FPSTR(HTML_COPYRIGHT);
  page += FPSTR(HTML_FOOTER);

  server.sendHeader("Content-Length", String(page.length()));
  server.send(200, "text/html", page);

  sendFlagAPRS = true;
}


void handleReset() {
  httpAuth();
  
  String page = FPSTR(HTML_HEADER);
  page.replace("{title}", " - Reset");
  page += FPSTR(HTML_STYLE);
  page += F("<script>setTimeout(function(){location.href='.';},9000);</script>");
  page += FPSTR(HTML_HEADER_END);
  page += String(F("<h3 align=\"center\">HTML</h3>"));
  page += F("La estacion meteorol&oacute;gica se reseteara en unos segundos.");
  page += FPSTR(HTML_COPYRIGHT);
  page += FPSTR(HTML_FOOTER);

  server.sendHeader("Content-Length", String(page.length()));
  server.send(200, "text/html", page);

  delay(5000);
  ESP.reset();
  delay(2000);
} // handleReset()


void handleNotFound() {
  String page;

  page = "Error";
  
  server.sendHeader(F("Content-Length"), String(page.length()));
  server.send(404, "text/html", page);
}


void httpAuth(void) {
  #ifdef HTTP_PROTECTED
  const char* username = HTTP_USERNAME;
  const char* password = HTTP_PASSWORD;
  if((!server.authenticate(username, password))) return server.requestAuthentication();
  #endif
} // httpAuth()
