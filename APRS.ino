/*
 * 
 * Funciones para el envio de tramas APRS
 * 
 */

void Send2APRS() {
  char login[60];
  char sentence[150];
  unsigned int randomnum = random(1000);// Numero aleatorio (ID del paquete de telemetria)

  /*
   * Reset Reason
   */
  /*
  char rr[13];
  String rrString = showResetReason(resetReason);
  int i=0,j=0;

  for(i=0;i<rrString.length();++i) rr[i] = rrString[i];
  for(j=i;j<13;j++) rr[j] = '\0';
  */

  // OBLIGATORIO: CWOP no necesita password, pero debe registrarse en el programa CWOP para obtener un indicativo valido
  sprintf(login, "user %s pass %i vers VERSION ESP8266", station.callsign, station.aprsPassword); // El usuario debe usar "CALLSING-13" si es un operador de radio, de lo contrario, solicitar y usar un indicativo CWOP...
   
  //retrieve telemetry infos
  tl.rssi = abs(WiFi.RSSI());              // strenght of WiFi AP signal
  tl.luminosidad = (int)ReadLuminosidad(); // Read from A0


  Log = "[WX]: Conectando al servidor APRS... "; 
  PRINTLOG;

  int retr = 20;
  while(!client.connect(station.aprsServerHost, station.aprsServerPort) && (retr > 0)) {
    delay(50);
    --retr;
  }
  
  if(!client.connected()) {
    Log = "ERROR!\n"; 
    PRINTLOG;
    client.stop();
    return;
  } else {  
    Log = "OK\n";
    PRINTLOG;
    client.println(login);
    Log  = login;
    Log += "\n";
    PRINTLOG;
    delay(3000); // 3 segundos de espera como lo recomendado entre el login y el envio de paquetes
  }
  
  // Print server reply
  while(client.available()) {
    String line = client.readStringUntil('\r');
    Log  = line;
    Log += "\n";
    PRINTLOG;
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Position Packet
  Log = "[WX]: POSITION PACKET\n"; 
  PRINTLOG;
  sprintf(sentence, "%s>APRS,TCPIP*:=%s/%s&%s (v%s) - %s - https://gitlab.com/ea1heh/APRSWXStation", station.callsign, 
                                                           station.latitude, 
                                                           station.longitude, 
                                                           station.aprsBeacon, 
                                                           VERSION,
                                                           uptime(false));
  client.println(sentence);
  Log  = sentence;
  Log += "\n"; 
  PRINTLOG;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Weather Packet
  Log = "[WX]: WEATHER PACKET\n"; 
  PRINTLOG;
  sprintf(sentence, "%s>APRS,TCPIP*:=%s/%s_.../...g...t%03dr...p...P...h%02db%05d", station.callsign, 
                                                                                    station.latitude, 
                                                                                    station.longitude, 
                                                                                    (int)(wx.temperatureF), 
                                                                                    (int)(wx.humidity),
                                                                             #ifdef SENSORBME280
                                                                                    (int)(wx.pressure*10));
                                                                             #endif
                                                                             #ifdef SENSORBMP280
                                                                                    (int)(wx.pressure/10));
                                                                             #endif
  client.println(sentence);
  Log  = sentence;
  Log += "\n"; 
  PRINTLOG;

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Telemetry Packet
  Log = "[WX]: TELEMETRY PACKET\n";
  PRINTLOG;
  sprintf(sentence, "%s>APRS,TCPIP*:T#%03d,%03d,%03d,000,000,000,00000000", station.callsign, 
                                                                            randomnum, 
                                                                            tl.rssi, 
                                                                            (int)tl.luminosidad);
  client.println(sentence);
  Log  = sentence;
  Log += "\n"; 
  PRINTLOG;
  

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Definir parametros de los paquetes de telemetria
  sprintf(sentence, "%s>APRS,TCPIP*::%s:PARM.RSSI,LX", station.callsign, 
                                                       station.tlm_callsign);
  client.println(sentence);
  Log  = sentence;
  Log += "\n"; 
  PRINTLOG;

  // Definir las unidades de los paquetes de telemetria
  sprintf(sentence, "%s>APRS,TCPIP*::%s:UNIT.dbm,%%", station.callsign, 
                                                      station.tlm_callsign);
  client.println(sentence);
  Log  = sentence;
  Log += "\n";
  PRINTLOG;

  // Añadir coeficiente de telemetria para que el protocolo APRS pueda convertir tus valores brutos en un valor real
  sprintf(sentence, "%s>APRS,TCPIP*::%s:EQNS.0,-1,0,0,0.01,0,0,0,0,0,0,0,0,0,0", station.callsign, 
                                                                                 station.tlm_callsign);
  client.println(sentence);
  Log  = sentence;
  Log += "\n";
  PRINTLOG;

  // Enviar bits y baliza - comentario
  sprintf(sentence, "%s>APRS,TCPIP*::%s:BITS.00000000,%s",  station.callsign, 
                                                            station.tlm_callsign, 
                                                            station.aprsBeacon);
  client.println(sentence);
  Log  = sentence;
  Log += "\n";
  PRINTLOG;

  Log = "\nCerrando conexion con el servidor APRS...\n\n";
  PRINTLOG;
  client.stop();
}
