 /*
  * 
  *  Funciones para el envio de paquetes al servidor MQTT
  *  
  */

#define MQTT_MAX_PACKET_SIZE       256
#define ARDUINOJSON_USE_LONG_LONG  1

#ifdef MQTT_ENABLE
void configureMQTT() {   
   mqttClient.setServer(mqttSRV.host, mqttSRV.port);
   //mqttClient.setCallback(mqttCallback);
   mqttReconnect();
}


void mqttReconnect() {
  Log = "[WX]: Intentando conectar a MQTT: "; 
  PRINTLOG;
  String clientId = "WXStation";
  clientId += String(ESP.getFlashChipId(), HEX);
    
  if(mqttClient.connect(clientId.c_str(), mqttSRV.username, mqttSRV.password)) {
    Log = "OK\n";
    PRINTLOG;
    //mqttClient.subscribe(mqttInTopic.c_str());
  } else {
    Log  = "ERROR! Codigo rc=";
    Log += mqttClient.state();
    Log += "\n";
    PRINTLOG;
  }
} // mqttReconnect()


void Send2MQTT() {
  String JSONmessageBuffer;
  StaticJsonDocument<MQTT_MAX_PACKET_SIZE> jsoninfo;

  float tmp;
  #ifdef SENSORBMP280
      tmp = (wx.pressure/100);
  #endif
  #ifdef SENSORBME280
      tmp = (wx.pressure);
  #endif
  
  jsoninfo["temp"] = wx.temperatureC;
  jsoninfo["hum"]  = wx.humidity;
  jsoninfo["pres"] = tmp;
  jsoninfo["lx"]   = tl.luminosidad;  
  jsoninfo["rssi"] = -tl.rssi;
      
  serializeJson(jsoninfo, JSONmessageBuffer);
      
  mqttClient.loop();
  if(!mqttClient.connected()) {
    mqttReconnect();
  }

  mqttClient.publish(mqttOutTopic.c_str(), JSONmessageBuffer.c_str());
}
#endif
